class FilmesController < ApplicationController
  before_action :set_filme, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!, except: [:index, :show]  # manda logar caso tente adicionar filme sem estar logado


  def index
    @filmes = Filme.all
  end

  def show
    @reviews = Review.where(filme_id: @filme.id).order("created_at DESC")

    if @review.blank?
      @avg_review = 0
    else
      @avg_review = @reviews.average(:rating).round(2)
    end
  end

  def new
    @filme = current_usuario.filmes.build
  end

  def edit
  end

  def create
    @filme = current_usuario.filmes.build(filme_params)

    respond_to do |format|
      if current_usuario.try(:admin?)
        @filme.save
        format.html { redirect_to @filme, notice: 'Filme was successfully created.' }
        format.json { render :show, status: :created, location: @filme }
      else
        format.html { render :new }
        format.json { render json: @filme.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /filmes/1
  # PATCH/PUT /filmes/1.json
  def update
    respond_to do |format|
      if current_usuario.try(:admin?)
        @filme.update(filme_params)
        format.html { redirect_to @filme, notice: 'Filme was successfully updated.' }
        format.json { render :show, status: :ok, location: @filme }
      else
        format.html { render :edit }
        format.json { render json: @filme.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /filmes/1
  # DELETE /filmes/1.json
  def destroy
    respond_to do |format|
      if current_usuario.try(:admin?)
        @filme.destroy
          format.html { redirect_to filmes_url, notice: 'Filme foi excluido com sucesso.' }
          format.json { head :no_content }
      else
          format.html { redirect_to filmes_url, notice: 'Permissão Negada' }
          format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_filme
      @filme = Filme.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def filme_params
      params.require(:filme).permit(:titulo, :descricao, :duracao, :diretor, :classificacao, :usuario_id, :image)
    end
end
