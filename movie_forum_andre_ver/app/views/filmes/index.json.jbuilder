json.array!(@filmes) do |filme|
  json.extract! filme, :id, :titulo, :descricao, :duracao, :diretor, :classificacao
  json.url filme_url(filme, format: :json)
end
