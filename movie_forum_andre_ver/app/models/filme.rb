class Filme < ActiveRecord::Base
  belongs_to :usuario
  has_many :reviews, dependent: :destroy

  has_attached_file :image, styles: { medium: "400x600#"}, default_url: "/assets/images/20-most-hilarious-movie-poster-remakes-15.jpg"  # inserir url default_url
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

end
