Rails.application.routes.draw do

  devise_for :usuarios
  resources :filmes do
  resources :reviews, except: [:show, :index]
  end

  root 'filmes#index'

end
