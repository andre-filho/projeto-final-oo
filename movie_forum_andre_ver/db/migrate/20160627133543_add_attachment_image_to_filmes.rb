class AddAttachmentImageToFilmes < ActiveRecord::Migration
  def self.up
    change_table :filmes do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :filmes, :image
  end
end
