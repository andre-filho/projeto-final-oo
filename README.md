---------------------------------------------
Universidade de Brasília - Campus UnB Gama
Orientação a Objetos - 2016.1
Exercicio de Programação 3 -- PROJETO FINAL

ANDRÉ DE SOUSA COSTA FILHO -> 150005521
MATHEUS BATISTA SILVA      -> 150018029

Tema: SITE/FÓRUM DE FILMES

Objetivo do Exercício: Implementar, desenvolvendo o conhecimento
                       da linguagem Ruby na plataforma Rails,
                       uma aplicação web onde usuários podem
                       expressar a sua opinião sobre filmes,
                       sejam clássicos ou recentes.
--------------------------------------------
# DESCRIÇÃO DAS CLASSES MODEL

    Filme
    Um objeto 'filme' pertence à um usuario (admin), e possui diversos comentários,
    que possuem depêndencia quanto à sua existência, assim, se um filme for excluido,
    seus comentários também serão excluidos.
    Um filme tambem possuium arquivo anexado, uma imagem com tamanho de corte e default
    pré-definidos, que é validada por seu tipo.
    
    Review
    Um review pertence à um filme e a um usuário. É o mais simples dos três e foi feito
    com um javascript (raty) o sistema de classificação por estrelas. Um review possui nota,
    dada em estrelas, e um comentário. Um review só pode ser escrito por um usuario cadastrado.
    
    Usuario
    O usuário foi criado a partir da devise com alguns módulos próprios que cobrem registro,
    autenticação, rastreamento, validação, entre outros. um usuário possui vários filmes e
    reviews. O último tem dependência de existência.Apenas um usuário autenticado (admin ou comum)
    pode colocar um review em um filme.
    
# COMO USAR A APLICAÇÃO
    
    1- Abra no terminal o repositório e mude para o diretório 
    $ cd movie_forum_andre_ver/
    depois,
    $ rails s
    
    2- Abra o navegador de sua preferência e entre na porta padrâo do rails
    (geralmente localhost:3000)
    
    3- Sem possuir login, pode-se visualizar os comentários de outros usuarios
    nos filmes criados pelo administrador. Para comentar é necessário estar logado.
    Os comentários e filmes só podem ser exluidos pelo administrador.
    
    4- Para atribuir um status de administrador à um usuário deve-se abrir o console do rails e
    digitar os seguintes comandos:
        Caso seja, por exemplo, o útlimo usuário cadastrado: "Usuario.last.update_attribute :admin, true". 
        Ou seja é necessário atualizar o atributo "admin" do Usuario, sendo que esse atributo é do tipo boolean.
    